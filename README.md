# Jahsse

A new implementation of an old game using Qt (pyside).

NOTE: this project was created as a training exercise only.

This application is not for profit: its use and distribution must be free, without any charges/costs for anyone.


## Development

Design the UI:
    `pyside6-designer mainwindow.ui`

Compile the UI into python code:
    `pyside6-uic mainwindow.ui -o mainwindow.py`


## Installation

TODO

## Usage

TODO

## Support

TODO

## Authors and acknowledgment

The icons for the dice were created by [Timplaru Emil](https://www.vecteezy.com/members/emiltimplaru)
and have been downloaded from [Vecteezy](https://www.vecteezy.com/png/9383735-casino-dice-clipart-design-illustration)

## License

GPLv3


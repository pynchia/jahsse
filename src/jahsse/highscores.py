# Only needed for access to command line arguments
import csv
import itertools as it


class HighScores:
    """Manage high scores.
    It loads the scores from the given score file upon creation and
    keeps the score file up to date when necessary
    """

    def __init__(self, pathname: str, max_size: int) -> None:
        """Instantiate the class to manage high scores

        Args:
            filename (str): the file where to load/store high scores
            max_size (int): how many entries to keep
        """
        self.pathname = pathname
        self.max_size = max_size
        self.hi_scores = []
        self.load_hi_scores()

    def load_hi_scores(self):
        with open(self.pathname, newline='') as f:
            reader = csv.reader(f, delimiter=';')
            for score, names in it.islice(reader, self.max_size):
                self.hi_scores.append([int(score), names])

    def write_hi_scores(self):
        with open(self.pathname, 'w', newline='') as f:
            writer = csv.writer(f, delimiter=';')
            writer.writerows(self.hi_scores[:self.max_size])

    def add_score_if_necessary(self, score: int, names: list[str]):
        if score > self.lowest_score:
            self.hi_scores[-1] = [score, ', '.join(names)]
            self.hi_scores.sort(reverse=True)
            self.write_hi_scores()

    def scores(self):
        yield from self.hi_scores

    @property
    def lowest_score(self) -> int:
        return self.hi_scores[-1][0]

# Only needed for access to command line arguments
import collections as co
import itertools as it
import pathlib
import random
import sys
import time
from typing import Callable

from PySide6.QtCore import QSize, Qt
from PySide6.QtGui import QPixmap
from PySide6.QtWidgets import (
    QApplication,
    QHeaderView,
    QInputDialog,
    QMainWindow,
    QMessageBox,
    QTableWidget,
    QTableWidgetItem,
)

from mainwindow import Ui_MainWindow
from highscores import HighScores


MAX_PLAYERS = 4
NUM_DICE = 5
NUM_STEPS = 13  # numbmer of choices to make in a game, per player
MAX_ROLLS = 3  # number of times each player can draw the dice for each step

# Main board
LOWER_HALF_ROW = 6

# Score board
UPPER_TOTAL_ROW = 0
BONUS_ROW = 1
LOWER_TOTAL_ROW = 2
EXTRA_BONUS_ROW = 3
TOTAL_ROW = 4
JAHSSE_ROW = 11

BONUS_VALUE = 35
BOUNUS_THRESHOLD = 63

ROLL_ANIMATION_DELAY = 0.2
ROLL_MSG = {
    0: ". Roll the dice",
    1: ", first roll. ",
    2: ", second roll. ",
    3: ". third and last roll. "
}

APP_BASEDIR = pathlib.Path(__file__).parent
IMG_DIR_NAME = APP_BASEDIR / 'img'
HI_SCORE_FILENAME =  APP_BASEDIR / "hi_scores.csv"
MAX_NAMES_IN_HI_SCORES = 10

hi_scores = HighScores(HI_SCORE_FILENAME, MAX_NAMES_IN_HI_SCORES)  # singleton


class CalcScoreByChoice:
    @staticmethod
    def calc_score_numbers(number: int, dice: list[int]) -> int:
        return sum(d for d in dice if d==number)

    @staticmethod
    def calc_score_equals(numerosity: int, dice: list[int]) -> int:
        d, c = co.Counter(dice).most_common(1)[0]  # the most common number and its numerosity
        return 0 if c < numerosity else sum(dice)

    @staticmethod
    def calc_score_full(dice: list[int]) -> int:
        cnt = co.Counter(dice)
        two_most_common = cnt.most_common(2)
        _, c1 = two_most_common[0]  # the most common number and its numerosity
        try:
            _, c2 = two_most_common[1]  # the 2nd most common number and its numerosity
        except IndexError:  # all the dice have the same value, it's a full nevertheless
            return 25
        return 25 if c1 == 3 and c2 == 2 else 0

    @staticmethod
    def calc_flush(numerosity: int, score_val:int, dice: list[int]) -> int:
        dice_wo_duplicates = set(dice)
        if len(dice_wo_duplicates) < numerosity:
            return 0
        ds = sorted(dice_wo_duplicates)
        for first_idx, first_val in enumerate(ds[:NUM_DICE-numerosity+1]):
            for i, d in enumerate(ds[first_idx:first_idx+numerosity], start=first_val):
                if i != d:
                    break
            else:
                return score_val
        return 0

CALC_SCORE_BY_CHOICE = [
    lambda dice: CalcScoreByChoice.calc_score_numbers(1, dice),
    lambda dice: CalcScoreByChoice.calc_score_numbers(2, dice),
    lambda dice: CalcScoreByChoice.calc_score_numbers(3, dice),
    lambda dice: CalcScoreByChoice.calc_score_numbers(4, dice),
    lambda dice: CalcScoreByChoice.calc_score_numbers(5, dice),
    lambda dice: CalcScoreByChoice.calc_score_numbers(6, dice),
    lambda dice: CalcScoreByChoice.calc_score_equals(3, dice),  # tris
    lambda dice: CalcScoreByChoice.calc_score_equals(4, dice),  # poker
    CalcScoreByChoice.calc_score_full,
    lambda dice: CalcScoreByChoice.calc_flush(4, 30, dice),  # min flush
    lambda dice: CalcScoreByChoice.calc_flush(5, 40, dice),  # max flush
    lambda dice: 50 if CalcScoreByChoice.calc_score_equals(5, dice) else 0,  # JAHSSE
    sum,
]


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        random.seed()
        self.setupUi(self)
        self.DICE_IMAGES = tuple(
            QPixmap(IMG_DIR_NAME / f"{i}.png")
            for i in range(7)
        )
        self.b_dice = [
            getattr(self, f"dice_{d}") for d in range(5)
        ]
        for die in self.b_dice:
            die.setStyleSheet(
                """
                QPushButton:checked { background-color: orangered; border: none; }
                QPushButton { border: none; }
                """
            )
            die.setIconSize(QSize(70, 70))
        self.menu_new_game_players = [
            getattr(self, f"action_{p}_player") for p in range(1, 5)
        ]
        for np, a in enumerate(self.menu_new_game_players, start=1):
            a.triggered.connect(
                lambda checked, np=np: self.new_game_of_n_players(num_players=np)
            )
        self.action_high_scores.triggered.connect(self.show_high_scores)
        self.board.verticalHeader().sectionClicked.connect(self.pick_choice)
        self.board.horizontalHeader().setSectionResizeMode(QHeaderView.Fixed)
        for row in range(NUM_STEPS):
            for col in range(MAX_PLAYERS):
                item = QTableWidgetItem("")
                item.setTextAlignment(
                    Qt.AlignmentFlag.AlignHCenter | Qt.AlignmentFlag.AlignVCenter
                )
                self.board.setItem(row, col, item)
        for row in range(5):
            for col in range(MAX_PLAYERS):
                self.score_board.item(row, col).setTextAlignment(
                    Qt.AlignmentFlag.AlignHCenter | Qt.AlignmentFlag.AlignVCenter
                )
        self.b_clear_dice.clicked.connect(self.clear_dice_selection)
        self.b_invert_dice.clicked.connect(self.invert_dice_selection)
        self.b_roll_dice.clicked.connect(self.roll_dice)
        self.status_bar = self.statusBar()
        self.new_game_of_n_players(1)
        self.show()

    def new_game_of_n_players(self, num_players):
        self.num_players = num_players
        self.next_player = it.cycle(range(num_players))
        self.current_player = 0
        self.current_step = 0
        self.dice_value = [0, 0, 0, 0, 0]
        self.bonus_achieved = [False]*num_players
        self.clear_board(self.board)
        self.clear_board(self.score_board)

        self.set_stage_for_new_turn()

    def set_stage_for_new_turn(self):
        for col in range(MAX_PLAYERS):
            self.select_deselect_col(col, sel=False)
        self.current_player = next(self.next_player)
        self.select_deselect_col(self.current_player, sel=True, empty_ones=True)
        self.current_roll = 0
        self.board.setDisabled(True)
        self.clear_dice_value()
        self.clear_dice_selection()
        self.disable_dice()
        self.b_clear_dice.setDisabled(True)
        self.b_invert_dice.setDisabled(True)
        self.b_roll_dice.setEnabled(True)
        self.status_msg = f"Player {str(self.current_player+1)}"
        self.status_bar.showMessage(self.status_msg+ROLL_MSG[self.current_roll])

    def show_high_scores(self):
        QMessageBox.information(
            self,
            "Jahsse Hall of fame",
            '\n'.join(f"{score:3d}\t{winners}         " for score, winners in hi_scores.scores())
        )

    def select_deselect_col(self, col:int, sel: bool, empty_ones: bool=False):
        for row in range(NUM_STEPS):
            if empty_ones:
                if self.board.item(row, col).text():
                    continue
            self.board.item(row, col).setSelected(sel)

    def disable_dice(self):
        for die in self.b_dice:
            die.setDisabled(True)

    def enable_dice(self):
        for die in self.b_dice:
            die.setEnabled(True)

    def clear_dice_value(self):
        for die in self.b_dice:
            die.setIcon(self.DICE_IMAGES[0])

    def clear_dice_selection(self):
        for die in self.b_dice:
            die.setChecked(False)

    def invert_dice_selection(self):
        for die in self.b_dice:
            die.setChecked(not die.isChecked())

    def roll_dice(self):
        for i, b_die in enumerate(self.b_dice):  # draw the new dice
            if not b_die.isChecked():
                self.dice_value[i] = val = random.randint(1, 6)
                b_die.setIcon(self.DICE_IMAGES[val])

        if self.current_roll == 0:
            self.board.setEnabled(True)
            self.enable_dice()
            self.b_clear_dice.setEnabled(True)
            self.b_invert_dice.setEnabled(True)
        self.current_roll += 1
        if self.current_roll == MAX_ROLLS:
            self.b_clear_dice.setDisabled(True)
            self.b_invert_dice.setDisabled(True)
            self.b_roll_dice.setDisabled(True)  # no more draws, you must choose a combination

        self.status_bar.showMessage(
            self.status_msg+ROLL_MSG[self.current_roll]+f"Sum of dice = {sum(self.dice_value)}"
        )

    @staticmethod
    def clear_board(board: QTableWidget):
        for row in range(board.rowCount()):
            for col in range(board.columnCount()):
                board.item(row, col).setText("")

    @staticmethod
    def calc_sum_within_col(
        board: QTableWidget,
        start_row: int,
        end_row:int,
        col: int,
        failed_row_callback: Callable[[int, str], int] = None
    ) -> int:
        """Sum all values in the given col of the given board
           within the given range or rows

        Args:
            board (QTableWidget): the board widget
            start_row (int): initial row
            end_row (int): upper limit (excluded, as in range)
            col (int): the column
            failed_row_callback (Callable): called when the value is not an int, passing it the row
                and the content of the cell. It must return a value for the cell.
                If not callback is specified the cell value is counted as zero

        Returns:
            int: the sum of such cell values
        """
        tot = 0
        for row in range(start_row, end_row):
            cell_content = board.item(row, col).text()
            try:
                val = int(cell_content)
            except ValueError:
                val = failed_row_callback(row, cell_content) if failed_row_callback is not None else 0
            tot += val
        return tot

    def determine_winners_and_score(self) -> tuple[list[int], int]:
        scores = [int(self.score_board.item(TOTAL_ROW, player).text()) for player in range(self.num_players)]
        max_score = max(scores)
        winners = []
        for player, score in enumerate(scores):
            if score == max_score:
                winners.append(player)
        return winners, max_score

    def pick_choice(self, row: int):
        if self.board.item(row, self.current_player).text():  # row has been chosen already
            return
        if self.board.item(JAHSSE_ROW, self.current_player).text() == "50":  # Jahsse had been done already
            if CalcScoreByChoice.calc_score_equals(5, self.dice_value) \
               and not self.score_board.item(EXTRA_BONUS_ROW, self.current_player).text():
                # it is the second Jahsse, so add an extra bonus
                self.score_board.item(EXTRA_BONUS_ROW, self.current_player).setText("100")

        score = CALC_SCORE_BY_CHOICE[row](self.dice_value)
        self.board.item(row, self.current_player).setText(str(score))  # set in board
        if row < 6:
            upper_total = self.calc_sum_within_col(self.board, 0, 6, self.current_player)
            self.score_board.item(UPPER_TOTAL_ROW, self.current_player).setText(str(upper_total))
            if upper_total >= BOUNUS_THRESHOLD:
                if not self.bonus_achieved[self.current_player]:
                    self.bonus_achieved[self.current_player] = True
                    self.score_board.item(BONUS_ROW, self.current_player).setText(str(BONUS_VALUE))
            else:
                target_estimate = self.calc_sum_within_col(
                    self.board, 0, 6, self.current_player, (lambda row, _: (row+1)*3)
                )
                target_msg = f"[{(target_estimate - BOUNUS_THRESHOLD):+d}]"
                self.score_board.item(BONUS_ROW, self.current_player).setText(target_msg)
        else:
            lower_total = self.calc_sum_within_col(self.board, LOWER_HALF_ROW, NUM_STEPS, self.current_player)
            self.score_board.item(LOWER_TOTAL_ROW, self.current_player).setText(str(lower_total))

        total = self.calc_sum_within_col(self.score_board, 0, TOTAL_ROW, self.current_player)
        self.score_board.item(TOTAL_ROW, self.current_player).setText(str(total))
        if self.current_player==(self.num_players-1):  # the last player
            self.current_step += 1
            if self.current_step==NUM_STEPS:  # end of game
                winners, score = self.determine_winners_and_score()
                optional_plural = 's are Players' if len(winners)>1 else ' is Player'
                winners_msg = f"the winner{optional_plural} {', '.join(map(lambda p: str(p+1), winners))}"
                self.status_bar.showMessage(
                    f"End of game, {winners_msg}"
                )
                if score > hi_scores.lowest_score:
                    # Ask for names of winners
                    names = []
                    for p in winners:
                        player_name, ok = QInputDialog.getText(self, winners_msg, f"Enter name of Player {p+1}:")
                        if not ok:
                            player_name = 'unknown'
                        names.append(player_name.upper())
                    hi_scores.add_score_if_necessary(score, names)
                return
        self.set_stage_for_new_turn()


app = QApplication(sys.argv)
window = MainWindow()
app.exec()
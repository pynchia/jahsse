# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 6.7.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QAction, QBrush, QColor, QConicalGradient,
    QCursor, QFont, QFontDatabase, QGradient,
    QIcon, QImage, QKeySequence, QLinearGradient,
    QPainter, QPalette, QPixmap, QRadialGradient,
    QTransform)
from PySide6.QtWidgets import (QAbstractItemView, QApplication, QHBoxLayout, QHeaderView,
    QMainWindow, QMenu, QMenuBar, QPushButton,
    QSizePolicy, QStatusBar, QTableWidget, QTableWidgetItem,
    QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(520, 800)
        self.action_1_player = QAction(MainWindow)
        self.action_1_player.setObjectName(u"action_1_player")
        self.action_2_player = QAction(MainWindow)
        self.action_2_player.setObjectName(u"action_2_player")
        self.actionThree_players = QAction(MainWindow)
        self.actionThree_players.setObjectName(u"actionThree_players")
        self.action_3_player = QAction(MainWindow)
        self.action_3_player.setObjectName(u"action_3_player")
        self.action_4_player = QAction(MainWindow)
        self.action_4_player.setObjectName(u"action_4_player")
        self.action_high_scores = QAction(MainWindow)
        self.action_high_scores.setObjectName(u"action_high_scores")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.dice_0 = QPushButton(self.centralwidget)
        self.dice_0.setObjectName(u"dice_0")
        self.dice_0.setGeometry(QRect(23, 430, 80, 80))
        self.dice_0.setCheckable(True)
        self.dice_1 = QPushButton(self.centralwidget)
        self.dice_1.setObjectName(u"dice_1")
        self.dice_1.setGeometry(QRect(123, 430, 80, 80))
        self.dice_1.setCheckable(True)
        self.dice_2 = QPushButton(self.centralwidget)
        self.dice_2.setObjectName(u"dice_2")
        self.dice_2.setGeometry(QRect(223, 430, 80, 80))
        self.dice_2.setCheckable(True)
        self.dice_3 = QPushButton(self.centralwidget)
        self.dice_3.setObjectName(u"dice_3")
        self.dice_3.setGeometry(QRect(323, 430, 80, 80))
        self.dice_3.setCheckable(True)
        self.dice_4 = QPushButton(self.centralwidget)
        self.dice_4.setObjectName(u"dice_4")
        self.dice_4.setGeometry(QRect(423, 430, 80, 80))
        self.dice_4.setCheckable(True)
        self.board = QTableWidget(self.centralwidget)
        if (self.board.columnCount() < 4):
            self.board.setColumnCount(4)
        __qtablewidgetitem = QTableWidgetItem()
        self.board.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.board.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.board.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.board.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        if (self.board.rowCount() < 13):
            self.board.setRowCount(13)
        __qtablewidgetitem4 = QTableWidgetItem()
        __qtablewidgetitem4.setTextAlignment(Qt.AlignCenter);
        self.board.setVerticalHeaderItem(0, __qtablewidgetitem4)
        __qtablewidgetitem5 = QTableWidgetItem()
        __qtablewidgetitem5.setTextAlignment(Qt.AlignCenter);
        self.board.setVerticalHeaderItem(1, __qtablewidgetitem5)
        __qtablewidgetitem6 = QTableWidgetItem()
        __qtablewidgetitem6.setTextAlignment(Qt.AlignCenter);
        self.board.setVerticalHeaderItem(2, __qtablewidgetitem6)
        __qtablewidgetitem7 = QTableWidgetItem()
        __qtablewidgetitem7.setTextAlignment(Qt.AlignCenter);
        self.board.setVerticalHeaderItem(3, __qtablewidgetitem7)
        __qtablewidgetitem8 = QTableWidgetItem()
        __qtablewidgetitem8.setTextAlignment(Qt.AlignCenter);
        self.board.setVerticalHeaderItem(4, __qtablewidgetitem8)
        __qtablewidgetitem9 = QTableWidgetItem()
        __qtablewidgetitem9.setTextAlignment(Qt.AlignCenter);
        self.board.setVerticalHeaderItem(5, __qtablewidgetitem9)
        __qtablewidgetitem10 = QTableWidgetItem()
        __qtablewidgetitem10.setTextAlignment(Qt.AlignCenter);
        self.board.setVerticalHeaderItem(6, __qtablewidgetitem10)
        __qtablewidgetitem11 = QTableWidgetItem()
        __qtablewidgetitem11.setTextAlignment(Qt.AlignCenter);
        self.board.setVerticalHeaderItem(7, __qtablewidgetitem11)
        __qtablewidgetitem12 = QTableWidgetItem()
        __qtablewidgetitem12.setTextAlignment(Qt.AlignCenter);
        self.board.setVerticalHeaderItem(8, __qtablewidgetitem12)
        __qtablewidgetitem13 = QTableWidgetItem()
        __qtablewidgetitem13.setTextAlignment(Qt.AlignCenter);
        self.board.setVerticalHeaderItem(9, __qtablewidgetitem13)
        __qtablewidgetitem14 = QTableWidgetItem()
        __qtablewidgetitem14.setTextAlignment(Qt.AlignCenter);
        self.board.setVerticalHeaderItem(10, __qtablewidgetitem14)
        __qtablewidgetitem15 = QTableWidgetItem()
        __qtablewidgetitem15.setTextAlignment(Qt.AlignCenter);
        self.board.setVerticalHeaderItem(11, __qtablewidgetitem15)
        __qtablewidgetitem16 = QTableWidgetItem()
        __qtablewidgetitem16.setTextAlignment(Qt.AlignCenter);
        self.board.setVerticalHeaderItem(12, __qtablewidgetitem16)
        self.board.setObjectName(u"board")
        self.board.setGeometry(QRect(0, 0, 511, 421))
        self.board.setEditTriggers(QAbstractItemView.EditTrigger.DoubleClicked)
        self.board.setTabKeyNavigation(False)
        self.board.setProperty("showDropIndicator", False)
        self.board.setDragDropOverwriteMode(False)
        self.board.setSelectionMode(QAbstractItemView.SelectionMode.NoSelection)
        self.board.setSelectionBehavior(QAbstractItemView.SelectionBehavior.SelectRows)
        self.board.setCornerButtonEnabled(False)
        self.layoutWidget = QWidget(self.centralwidget)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(22, 528, 481, 27))
        self.horizontalLayout = QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.b_clear_dice = QPushButton(self.layoutWidget)
        self.b_clear_dice.setObjectName(u"b_clear_dice")

        self.horizontalLayout.addWidget(self.b_clear_dice)

        self.b_invert_dice = QPushButton(self.layoutWidget)
        self.b_invert_dice.setObjectName(u"b_invert_dice")

        self.horizontalLayout.addWidget(self.b_invert_dice)

        self.b_roll_dice = QPushButton(self.layoutWidget)
        self.b_roll_dice.setObjectName(u"b_roll_dice")

        self.horizontalLayout.addWidget(self.b_roll_dice)

        self.score_board = QTableWidget(self.centralwidget)
        if (self.score_board.columnCount() < 4):
            self.score_board.setColumnCount(4)
        __qtablewidgetitem17 = QTableWidgetItem()
        self.score_board.setHorizontalHeaderItem(0, __qtablewidgetitem17)
        __qtablewidgetitem18 = QTableWidgetItem()
        self.score_board.setHorizontalHeaderItem(1, __qtablewidgetitem18)
        __qtablewidgetitem19 = QTableWidgetItem()
        self.score_board.setHorizontalHeaderItem(2, __qtablewidgetitem19)
        __qtablewidgetitem20 = QTableWidgetItem()
        self.score_board.setHorizontalHeaderItem(3, __qtablewidgetitem20)
        if (self.score_board.rowCount() < 5):
            self.score_board.setRowCount(5)
        __qtablewidgetitem21 = QTableWidgetItem()
        __qtablewidgetitem21.setTextAlignment(Qt.AlignCenter);
        self.score_board.setVerticalHeaderItem(0, __qtablewidgetitem21)
        __qtablewidgetitem22 = QTableWidgetItem()
        __qtablewidgetitem22.setTextAlignment(Qt.AlignCenter);
        self.score_board.setVerticalHeaderItem(1, __qtablewidgetitem22)
        __qtablewidgetitem23 = QTableWidgetItem()
        __qtablewidgetitem23.setTextAlignment(Qt.AlignCenter);
        self.score_board.setVerticalHeaderItem(2, __qtablewidgetitem23)
        __qtablewidgetitem24 = QTableWidgetItem()
        __qtablewidgetitem24.setTextAlignment(Qt.AlignCenter);
        self.score_board.setVerticalHeaderItem(3, __qtablewidgetitem24)
        __qtablewidgetitem25 = QTableWidgetItem()
        __qtablewidgetitem25.setTextAlignment(Qt.AlignCenter);
        self.score_board.setVerticalHeaderItem(4, __qtablewidgetitem25)
        __qtablewidgetitem26 = QTableWidgetItem()
        self.score_board.setItem(0, 0, __qtablewidgetitem26)
        __qtablewidgetitem27 = QTableWidgetItem()
        self.score_board.setItem(0, 1, __qtablewidgetitem27)
        __qtablewidgetitem28 = QTableWidgetItem()
        self.score_board.setItem(0, 2, __qtablewidgetitem28)
        __qtablewidgetitem29 = QTableWidgetItem()
        self.score_board.setItem(0, 3, __qtablewidgetitem29)
        __qtablewidgetitem30 = QTableWidgetItem()
        self.score_board.setItem(1, 0, __qtablewidgetitem30)
        __qtablewidgetitem31 = QTableWidgetItem()
        self.score_board.setItem(1, 1, __qtablewidgetitem31)
        __qtablewidgetitem32 = QTableWidgetItem()
        self.score_board.setItem(1, 2, __qtablewidgetitem32)
        __qtablewidgetitem33 = QTableWidgetItem()
        self.score_board.setItem(1, 3, __qtablewidgetitem33)
        __qtablewidgetitem34 = QTableWidgetItem()
        self.score_board.setItem(2, 0, __qtablewidgetitem34)
        __qtablewidgetitem35 = QTableWidgetItem()
        self.score_board.setItem(2, 1, __qtablewidgetitem35)
        __qtablewidgetitem36 = QTableWidgetItem()
        self.score_board.setItem(2, 2, __qtablewidgetitem36)
        __qtablewidgetitem37 = QTableWidgetItem()
        self.score_board.setItem(2, 3, __qtablewidgetitem37)
        __qtablewidgetitem38 = QTableWidgetItem()
        self.score_board.setItem(3, 0, __qtablewidgetitem38)
        __qtablewidgetitem39 = QTableWidgetItem()
        self.score_board.setItem(3, 1, __qtablewidgetitem39)
        __qtablewidgetitem40 = QTableWidgetItem()
        self.score_board.setItem(3, 2, __qtablewidgetitem40)
        __qtablewidgetitem41 = QTableWidgetItem()
        self.score_board.setItem(3, 3, __qtablewidgetitem41)
        __qtablewidgetitem42 = QTableWidgetItem()
        self.score_board.setItem(4, 0, __qtablewidgetitem42)
        __qtablewidgetitem43 = QTableWidgetItem()
        self.score_board.setItem(4, 1, __qtablewidgetitem43)
        __qtablewidgetitem44 = QTableWidgetItem()
        self.score_board.setItem(4, 2, __qtablewidgetitem44)
        __qtablewidgetitem45 = QTableWidgetItem()
        self.score_board.setItem(4, 3, __qtablewidgetitem45)
        self.score_board.setObjectName(u"score_board")
        self.score_board.setEnabled(True)
        self.score_board.setGeometry(QRect(0, 578, 511, 181))
        self.score_board.setAutoScroll(False)
        self.score_board.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers)
        self.score_board.setTabKeyNavigation(False)
        self.score_board.setProperty("showDropIndicator", False)
        self.score_board.setDragDropOverwriteMode(False)
        self.score_board.setSelectionMode(QAbstractItemView.SelectionMode.NoSelection)
        self.score_board.setSelectionBehavior(QAbstractItemView.SelectionBehavior.SelectRows)
        self.score_board.setCornerButtonEnabled(False)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 520, 22))
        self.menuFile = QMenu(self.menubar)
        self.menuFile.setObjectName(u"menuFile")
        self.menuHighScores = QMenu(self.menubar)
        self.menuHighScores.setObjectName(u"menuHighScores")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuHighScores.menuAction())
        self.menuFile.addAction(self.action_1_player)
        self.menuFile.addAction(self.action_2_player)
        self.menuFile.addAction(self.action_3_player)
        self.menuFile.addAction(self.action_4_player)
        self.menuHighScores.addAction(self.action_high_scores)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Jahsse", None))
        self.action_1_player.setText(QCoreApplication.translate("MainWindow", u"One player", None))
        self.action_2_player.setText(QCoreApplication.translate("MainWindow", u"Two players", None))
        self.actionThree_players.setText(QCoreApplication.translate("MainWindow", u"Three players", None))
        self.action_3_player.setText(QCoreApplication.translate("MainWindow", u"Three players", None))
        self.action_4_player.setText(QCoreApplication.translate("MainWindow", u"Four players", None))
        self.action_high_scores.setText(QCoreApplication.translate("MainWindow", u"High scores", None))
        self.dice_0.setText("")
        self.dice_1.setText("")
        self.dice_2.setText("")
        self.dice_3.setText("")
        self.dice_4.setText("")
        ___qtablewidgetitem = self.board.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("MainWindow", u"Player 1", None));
        ___qtablewidgetitem1 = self.board.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("MainWindow", u"Player 2", None));
        ___qtablewidgetitem2 = self.board.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("MainWindow", u"Player 3", None));
        ___qtablewidgetitem3 = self.board.horizontalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("MainWindow", u"Player 4", None));
        ___qtablewidgetitem4 = self.board.verticalHeaderItem(0)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("MainWindow", u"Ones", None));
        ___qtablewidgetitem5 = self.board.verticalHeaderItem(1)
        ___qtablewidgetitem5.setText(QCoreApplication.translate("MainWindow", u"Twos", None));
        ___qtablewidgetitem6 = self.board.verticalHeaderItem(2)
        ___qtablewidgetitem6.setText(QCoreApplication.translate("MainWindow", u"Threes", None));
        ___qtablewidgetitem7 = self.board.verticalHeaderItem(3)
        ___qtablewidgetitem7.setText(QCoreApplication.translate("MainWindow", u"Fours", None));
        ___qtablewidgetitem8 = self.board.verticalHeaderItem(4)
        ___qtablewidgetitem8.setText(QCoreApplication.translate("MainWindow", u"Fives", None));
        ___qtablewidgetitem9 = self.board.verticalHeaderItem(5)
        ___qtablewidgetitem9.setText(QCoreApplication.translate("MainWindow", u"Sixes", None));
        ___qtablewidgetitem10 = self.board.verticalHeaderItem(6)
        ___qtablewidgetitem10.setText(QCoreApplication.translate("MainWindow", u"Tris", None));
        ___qtablewidgetitem11 = self.board.verticalHeaderItem(7)
        ___qtablewidgetitem11.setText(QCoreApplication.translate("MainWindow", u"Poker", None));
        ___qtablewidgetitem12 = self.board.verticalHeaderItem(8)
        ___qtablewidgetitem12.setText(QCoreApplication.translate("MainWindow", u"Full", None));
        ___qtablewidgetitem13 = self.board.verticalHeaderItem(9)
        ___qtablewidgetitem13.setText(QCoreApplication.translate("MainWindow", u"Min", None));
        ___qtablewidgetitem14 = self.board.verticalHeaderItem(10)
        ___qtablewidgetitem14.setText(QCoreApplication.translate("MainWindow", u"Max", None));
        ___qtablewidgetitem15 = self.board.verticalHeaderItem(11)
        ___qtablewidgetitem15.setText(QCoreApplication.translate("MainWindow", u"Jahsse", None));
        ___qtablewidgetitem16 = self.board.verticalHeaderItem(12)
        ___qtablewidgetitem16.setText(QCoreApplication.translate("MainWindow", u"Chance", None));
        self.b_clear_dice.setText(QCoreApplication.translate("MainWindow", u"Clear selection", None))
        self.b_invert_dice.setText(QCoreApplication.translate("MainWindow", u"Invert selection", None))
        self.b_roll_dice.setText(QCoreApplication.translate("MainWindow", u"Roll dice", None))
        ___qtablewidgetitem17 = self.score_board.horizontalHeaderItem(0)
        ___qtablewidgetitem17.setText(QCoreApplication.translate("MainWindow", u"Player 1", None));
        ___qtablewidgetitem18 = self.score_board.horizontalHeaderItem(1)
        ___qtablewidgetitem18.setText(QCoreApplication.translate("MainWindow", u"Player 2", None));
        ___qtablewidgetitem19 = self.score_board.horizontalHeaderItem(2)
        ___qtablewidgetitem19.setText(QCoreApplication.translate("MainWindow", u"Player 3", None));
        ___qtablewidgetitem20 = self.score_board.horizontalHeaderItem(3)
        ___qtablewidgetitem20.setText(QCoreApplication.translate("MainWindow", u"Player 4", None));
        ___qtablewidgetitem21 = self.score_board.verticalHeaderItem(0)
        ___qtablewidgetitem21.setText(QCoreApplication.translate("MainWindow", u"Upper Total", None));
        ___qtablewidgetitem22 = self.score_board.verticalHeaderItem(1)
        ___qtablewidgetitem22.setText(QCoreApplication.translate("MainWindow", u"Bonus", None));
        ___qtablewidgetitem23 = self.score_board.verticalHeaderItem(2)
        ___qtablewidgetitem23.setText(QCoreApplication.translate("MainWindow", u"Lower Total", None));
        ___qtablewidgetitem24 = self.score_board.verticalHeaderItem(3)
        ___qtablewidgetitem24.setText(QCoreApplication.translate("MainWindow", u"Extra Bonus", None));
        ___qtablewidgetitem25 = self.score_board.verticalHeaderItem(4)
        ___qtablewidgetitem25.setText(QCoreApplication.translate("MainWindow", u"Total", None));

        __sortingEnabled = self.score_board.isSortingEnabled()
        self.score_board.setSortingEnabled(False)
        ___qtablewidgetitem26 = self.score_board.item(0, 0)
        ___qtablewidgetitem26.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem27 = self.score_board.item(0, 1)
        ___qtablewidgetitem27.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem28 = self.score_board.item(0, 2)
        ___qtablewidgetitem28.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem29 = self.score_board.item(0, 3)
        ___qtablewidgetitem29.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem30 = self.score_board.item(1, 0)
        ___qtablewidgetitem30.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem31 = self.score_board.item(1, 1)
        ___qtablewidgetitem31.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem32 = self.score_board.item(1, 2)
        ___qtablewidgetitem32.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem33 = self.score_board.item(1, 3)
        ___qtablewidgetitem33.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem34 = self.score_board.item(2, 0)
        ___qtablewidgetitem34.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem35 = self.score_board.item(2, 1)
        ___qtablewidgetitem35.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem36 = self.score_board.item(2, 2)
        ___qtablewidgetitem36.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem37 = self.score_board.item(2, 3)
        ___qtablewidgetitem37.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem38 = self.score_board.item(3, 0)
        ___qtablewidgetitem38.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem39 = self.score_board.item(3, 1)
        ___qtablewidgetitem39.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem40 = self.score_board.item(3, 2)
        ___qtablewidgetitem40.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem41 = self.score_board.item(3, 3)
        ___qtablewidgetitem41.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem42 = self.score_board.item(4, 0)
        ___qtablewidgetitem42.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem43 = self.score_board.item(4, 1)
        ___qtablewidgetitem43.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem44 = self.score_board.item(4, 2)
        ___qtablewidgetitem44.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem45 = self.score_board.item(4, 3)
        ___qtablewidgetitem45.setText(QCoreApplication.translate("MainWindow", u"0", None));
        self.score_board.setSortingEnabled(__sortingEnabled)

        self.menuFile.setTitle(QCoreApplication.translate("MainWindow", u"New Game", None))
        self.menuHighScores.setTitle(QCoreApplication.translate("MainWindow", u"High scores", None))
    # retranslateUi

